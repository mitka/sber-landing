const gulp = require("gulp");
const htmlmin = require("gulp-htmlmin");
const del = require("del");
const postcss = require("gulp-postcss");

const paths = {
  png: "src/**/*.png",
  svg: "src/**/*.svg",
  styles: "src/**/*.css",
  html: "src/**/*.html",
  dest: "./target"
};

const clean = () => del([paths.dest]);

const minifyHTML = () =>
  gulp
    .src(paths.html)
    .pipe(
      htmlmin({
        ignoreCustomFragments: [/{{.*?}}/],
        collapseWhitespace: true,
        removeAttributeQuotes: true,
        removeComments: true,
        minifyJS: true
      })
    )
    .pipe(gulp.dest(paths.dest));

const minifyCss = () =>
  gulp
    .src(paths.styles)
    .pipe(
      postcss([
        require("autoprefixer")({
          //   browsers: ["last 2 versions", "ie >= 10"],
          grid: true
        }),
        require("cssnano")
      ])
    )
    .pipe(gulp.dest(paths.dest));

const build = gulp.series(clean, gulp.parallel(minifyHTML, minifyCss));

exports.default = build;
